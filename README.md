Tokybook Audio Book Downloader
==========================

Download chapterized mp3 audiobooks from tokybook.com

Usage:

```
  ./tokydl -slug book-slug-from-url
```

Where the slug is the part of the url after the domain, for example, to download:

> https://tokybook.com/midnight-sun

run

```
  ./tokydl -slug midnight-sun
```
