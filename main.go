package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
	"sync"
)

const TOKY_URL = "https://tokybook.com/"
const FETCH_URL = "https://api.crystallization.tv/api-us/getMp3Link"
const CHAPTER_REGEX = `"chapter_id":[ ]*"\d+"`
const JSON_BODY = `{"chapterId": %v, "serverType": 1}`

type res struct {
	url string
	err error
}

type link struct {
	Duration int    `json:"duration"`
	Link     string `json:"link_mp3"`
}

func getChapterIds(src string) []string {
	var out []string
	r := regexp.MustCompile(CHAPTER_REGEX)
	matches := r.FindAllString(src, -1)
	for _, m := range matches {
		out = append(out, parseChapterId(m))
	}
	return out
}

func parseChapterId(raw string) string {
	fields := strings.Split(raw, ":")
	return fields[len(fields)-1]
}

func getMp3Link(id string) (string, error) {
	jsonBody := []byte(fmt.Sprintf(JSON_BODY, id))
	reqBody := bytes.NewReader(jsonBody)

	url := fmt.Sprintf(FETCH_URL)
	resp, err := http.Post(url, "application/json", reqBody)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	var linkRes link
	json.Unmarshal(body, &linkRes)

	// fmt.Println("fetched mp3 link for chapter id:", id)

	return linkRes.Link, nil
}

func getBookTitle(src string) (string, error) {
	r := regexp.MustCompile("<h1>\n.+")
	m := r.FindString(src)
	fields := strings.Split(m, "\n")
	return fields[len(fields)-1], nil
}

func getSlugSource(slug string) (string, error) {
	resp, err := http.Get(TOKY_URL + slug)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

func downloadChapters(path string, ids ...string) error {
	var links []string
	for _, id := range ids {
		link, err := getMp3Link(id)
		if err != nil {
			return err
		}
		links = append(links, link)
	}
	resCh := make(chan res, len(links))

	var wg sync.WaitGroup
	for _, w := range links {
		wg.Add(1)

		go func(w string) {
			defer wg.Done()
			downloadFile(path, w, resCh)
		}(w)
	}

	wg.Wait()

	return nil
}

// download a file to a path on disk
func downloadFile(downloadPath string, fileUrl string, resCh chan (res)) {
	filePath, err := url.Parse(fileUrl)
	if err != nil {
		fmt.Fprintln(os.Stderr, "failed to parse url:", err)
		resCh <- res{fileUrl, err}
		return
	}
	fileSegments := strings.Split(filePath.Path, "/")
	fileName := downloadPath + "/" + fileSegments[len(fileSegments)-1]

	file, err := os.Create(fileName)
	if err != nil {
		fmt.Fprintln(os.Stderr, "failed to create local file:", err)
		resCh <- res{fileUrl, err}
		return
	}
	defer file.Close()

	client := http.Client{
		CheckRedirect: func(r *http.Request, via []*http.Request) error {
			r.URL.Opaque = r.URL.Path
			return nil
		},
	}

	resp, err := client.Get(fileUrl)
	if err != nil {
		fmt.Fprintln(os.Stderr, "failed to download file:", err)
		resCh <- res{fileUrl, err}
		return
	}
	defer resp.Body.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		fmt.Fprintln(os.Stderr, "failed to copy data to local file:", err)
		resCh <- res{fileUrl, err}
		return
	}

	fmt.Println("downloaded", fileUrl)
	resCh <- res{fileUrl, nil}
	return
}

func main() {
	// fmt.Println("Toky Audiobook Downloader v0.1.0")
	slug := flag.String("slug", "", "book url slug")

	flag.Parse()

	if *slug == "" {
		fmt.Println("please provide a book url slug to download")
		os.Exit(1)
	}

	src, err := getSlugSource(*slug)
	if err != nil {
		fmt.Println("error fetching book source:", err)
		os.Exit(1)
	}

	title, err := getBookTitle(src)
	if err != nil {
		fmt.Println("error reading book title:", err)
		os.Exit(1)
	}

	// create download directory
	if _, err := os.Stat("./" + title); errors.Is(err, os.ErrNotExist) {
		err := os.Mkdir("./"+title, 0755)
		if err != nil {
			fmt.Fprintln(os.Stderr, "failed to create download directory:", err)
		}
	}

	chapterIds := getChapterIds(src)
	if err := downloadChapters("./"+title, chapterIds...); err != nil {
		fmt.Println("error during download:", err)
		os.Exit(1)
	}

}
